# Guild application to join the Suboptimal alliance!

Please only fill this out if you are the guildmaster of the guild in question! All fields are required unless otherwise specified.

If you like, you may use the raw [Markdown](https://en.wikipedia.org/wiki/Markdown) that makes up this document as a template for your application (it should work in, for example, Discord™).

**\*** Asterisked fields are not individually required, but you **_must_** provide answers to **at least two** of them (the more, the merrier).

- **Your name (what you prefer to be called):**
- **Your guild’s name:**
- **What defines your guild, and how does it fit into Suboptimal (feel free to use as many paragraphs as you need or see fit)?:**
- **Any additional rules that your guild has that are non-obvious, if any:**
- **Size of your guild, and roughly what proportion of that is active:**
- **Age of your guild (roughly):**
- **Any geographical/linguistic considerations for your guild (e.g. “mostly UTC+8 players”, “speaks mostly English \& Spanish”, etc.), if applicable:**
- **Dedicated out-of-game contact/social modes for your guild, if any:**
- **How do you feel about your guild’s membership in Suboptimal being experimental, given that the alliance is very limited on guild slots?:**
- **IGN of your guildmaster:**
- **Other IGNs that you play actively (level \& job are optional), if any:**
- **What is your timezone/availability like?:**
- **\*Your Discord™ identifier (case-sensitive \& includes four-digit discriminator, e.g. `deer#1028`):**
- **\*Your email (includes server domain after the `@`, e.g. `capreolina[put an at sign here!]protonmail.ch`):**
- **\*Your matrix (includes server domain after the `:`, e.g. `d33r:matrix.org`):**
- **\*One or more additional methods of contacting you:**
- **Are you actively able to be contacted via one or more of the above methods? (yes/no):**
- **Your MapleLegends forum ID (optional):**
- **Any additional considerations, or anything else you’d like to add :\] ?:**

Thank you!!
