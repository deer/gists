# My settings for recording MapleLegends

## MapleLegends

### Legends\.ini

```ini
[APPEARANCE]
; This sets your resolution for MapleLegends
; 0 = 800x600
; 1 = 1024x768
; 2 = 1366x768 (Unstable on MAC)
HDClient = 2

; Run client in windowed mode
; Put on false if you prefer starting in full-screen mode
; In-game you can also press ALT + ENTER to toggle between windowed and full-screen
Windowed = true

; Enable darker chat background, which helps you seeing the chat in bright areas
DarkChat = true

; Enable darker Quest alarm background, which makes quests data visible on all backgrounds.
DarkQuestAlarm = true

; Enable the Streamer Mode, which hides the login ID in the Title screen and in the Cash Shop.
StreamerMode = true

; Set to false to hide the visual effects that would show when swinging a cosmetic Weapon cover.
WeaponEffects = false

[PERFORMANCE]
; Select the type of transitioning between two maps. This affects the duration and speed of the dark screen during map transfer.
; 1 = Classic
; 2 = Modern GMS-like
; 3 = Quicker
Transition = 3

[GAME]
; Adds the level of a monster next to its name tag, under the sprite.
; Set to false to keep only the monster name.
AddMonsterLevelTag = true
```

### In-game settings

- SYSTEM OPTIONS
    - <b>PICTURE QUALITY:</b> BEST
    - BGM
        - <b>MUTE:</b> true
    - SOUND
        - <b>MUTE:</b> false
    - <b>SHAKE UP THE SCREEN:</b> false
    - <b>MONSTER INFO.:</b> Mark name and HP

## OBS

### Settings

- Output
    - Recording
        - <b>Type:</b> Standard
        - Recording Settings
            - <b>Recording Format:</b> [mkv](https://en.wikipedia.org/wiki/Matroska)
            - <b>Encoder:</b> FFmpeg VAAPI H\.264 \[Using [VA-API](https://en.wikipedia.org/wiki/Video_Acceleration_API) allows me to use my GPU for encoding.\]
            - <b>Rescale Output:</b> false
            - <b>Custom Muxer Settings:</b> \[<i>none</i>\]
            - <b>Automatic File Splitting:</b> false
        - Encoder Settings
            - <b>Profile:</b> High \[The most widely-used [AVC profile](https://en.wikipedia.org/wiki/Advanced_Video_Coding#Profiles), and also the highest-quality one that I can even choose.\]
            - <b>Level:</b> 5\.2 \[The highest [level](https://en.wikipedia.org/wiki/Advanced_Video_Coding#Levels) that I can choose.\]
            - <b>Rate Control:</b> CQP \[CRF is better than CQP, but I have no CRF option, so CQP it is.\]
            - <b>QP:</b> 16 \[Lower means higher quality &amp; larger output, and vice versa. 16 is a bit greedily low, but it can go down as low as 0. Something like &ap;19 is conventionally considered to be “visually transparent” for more typical (read: _not_ animated pixel art…) inputs.\]
            - <b>Keyframe Interval (0=auto):</b> 0&nbsp;s \[This allows the codec to choose keyframes as it sees fit.\]
    - Audio
        - Track `n`
            - <b>Audio Bitrate:</b> 320 \[320 kb&#x29f8;s is the highest bitrate available with [AAC](https://en.wikipedia.org/wiki/Advanced_Audio_Coding).\]
- Audio
    - General
        - <b>Sample Rate:</b> 44\.1&nbsp;kHz
        - <b>Channels:</b> Stereo
- Video
    - <b>Base (Canvas) Resolution:</b> 1376x774 \[The smallest 16&ratio;9 resolution that is at least as large along both dimensions as the game’s display resolution.\]
    - <b>Output (Scaled) Resolution:</b> 1376x774
    - <b>Downscale Filter:</b> \[<i>none</i>\]
    - <b>FPS:</b> 60
- Advanced
    - Video
        - <b>Color Format:</b> NV12 (8-bit, 4:2:0, 2 planes)
        - <b>Color Space:</b> Rec. 709
        - <b>Color Range:</b> Limited

### Canvas layout

Just a “Window Capture” of the MapleLegends window. With this selected (in
OBS’s visual preview), you can right click &#x27a1;&#xfe0f; “Transform” &#x27a1;&#xfe0f; “Edit Transform…” to
get to the “Edit Transform” window. The settings here are:

- <b>Position:</b> (0,&nbsp;0)
- <b>Rotation:</b> 0
- <b>Size:</b> (1366&nbsp;px,&nbsp;768&nbsp;px)
- <b>Positional Alignment:</b> Top Left
- <b>Bounding Box Type:</b> No bounds
- Crop
    - <b>Left:</b> 0
    - <b>Right:</b> 0
    - <b>Top:</b> 0
    - <b>Bottom:</b> 0
