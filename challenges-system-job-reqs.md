# Make the Challenges System level-gated (level ≥100) instead of job-advancement-gated

Currently, the [Challenges System](https://forum.maplelegends.com/index.php?threads/alpha-test-challenges-system-hp-wash-alternative.47420/), a new mechanic that is intended to replace HP washing for [PC](https://en.wikipedia.org/wiki/Player_character)s who cannot or will not wash, requires the PC to be either 3rd-job or 4th-job. **This restriction is not only completely unnecessary, but is actively harmful, and often totally excludes those who need the Challenges System the most.**

The Challenges System is _already_ level-gated (it starts at level 100 at the lowest). The fact that it _also_ gatekeeps PCs based on their job is thus redundant at best. So why is the job requirement there?

I suspect — but cannot prove — that the job requirement is intended to mitigate theoretical (or pre-theoretical/speculative) exploits similar to the so-called “beginner-mage” build, wherein a PC job advances very late in order to benefit from HP gains that they can only obtain (or can more easily obtain) at jobs lower (i\.e. lower grade, e\.g. beginner&nbsp;\<&nbsp;bishop) than their intended target job (the “intended target job” being some kind of 4th-job mage, in this case).

However, there is apparently(?) some other reason, because if this was actually the concern, then the Challenges System would only need to be gated to _2nd_ job or higher, rather than the _3rd_ job or higher that was chosen. Nevertheless, we will show that it is easy to incorporate 0th-jobbers, 1st-jobbers, and 2nd-jobbers alike into the Challenges System, thus removing the harmful &amp; unnecessary built-in discrimination.

## How?

There are two parts to this change, although the second part is not really a “change” so much as it is a _clarification_.

### Part 1: Voluntary job-locking

This part is simple: If anyone who gains MAXHP from a challenge is thenceforth permanently prohibited from taking job advancements — with the obvious &amp; important exception of 4th job advancement! — then the issues are removed.

Here’s a little Q&amp;A for part 1:

- <b>Q:</b> Isn’t doing a quest that permanently _prevents_ job advancement pretty weird?
    - <b>A:</b> Yes, but here’s something even weirder: doing such a quest (challenge) _already permanently prevents HP washing_, and indeed prevents the ability to put anything into MAXHP/MAXMP at all! This would be the first time that a quest interferes with MAXHP/MAXMP allocation, but _not_ the first time that a quest interferes with job advancement: job advancement <b>quests</b> cause job advancement when fully completed, and such advancements are both irreversible (e\.g. a PC who advances from magician to I/L wizard can never go back and choose to be a magician again), &amp; eliminate higher advancement possibilities (e\.g. a PC who advances from magician to I/L wizard cannot choose to advance from I/L wizard to F/P mage at 3rd job advancement).

      Also remember that doing the challenges — and accepting _specifically the MAXHP rewards_ — would be totally voluntary, and players would be warned in advance that they are preventing themselves from job-advancing.
- <b>Q:</b> What if I negligently forget that accepting MAXHP from a challenge prevents me from job-advancing, and I ignore all warnings, and then I permanently screw up my character?
    - <b>A:</b> This can only happen if you are **both** level ≥100 **and** are a 2nd-jobber, 1st-jobber, or 0th-jobber (beginner). If you get to this point, you are _at least_ 30 levels late for your job advancement, and you certainly know what you are doing. The risk of permanently screwing up your character by inadvertently (or purposely, only to find out later that it was a bad idea) disabling HP washing for yourself is far, far, _**far**_ more likely.
- <b>Q:</b> Isn’t this overly strict?
    - <b>A:</b> Yes, it can be loosened by _only_ disabling 1st &amp; 2nd job advancements (advancement from 0th job to 1st job, and advancement from 1st job to 2nd job), as these are the problematic transitions. The HP thresholds (which are what dictate how much MAXHP you can gain from the system) do not observe distinctions beyond this point, with only two exceptions:
        - It treats brawlers(/marauders/buccaneers) differently from pirates(/gunslingers/outlaws/corsairs). This one is simply a non-issue, as brawlers/marauders/buccaneers have the [Improve MaxHP](https://maplelegends.com/lib/skill?id=5100000) skill, so their HP thresholds are much higher and have nothing to gain from “beginner-mage”-style exploits.
        - It treats bandits(/CBs/shadowers) differently from rogues(/assassins/hermits/nightlords). This one is actually significant, as a rogue could hypothetically put off 2nd job advancement until some level ≥110 of their choosing, in order to benefit from the “other” category of HP threshold, which is more advantageous (read: larger at every level from 110 onwards) than the “shad” category. Then, they would take 2nd (and 3rd &amp; 4th, in most cases) job advancement once they reach their MAXHP goal.\* This means that, <i>ceteris paribus</i>, eliminating the ability to do 2nd job advancement upon receiving MAXHP from a challenge **is** necessary.

      Furthermore, it’s possible to loosen things even further by only preventing the PC from making the specifically problematic job advancements, namely &langle;Beginner&nbsp;&rarr;&nbsp;Magician&rangle; and &langle;Rogue&nbsp;&rarr;&nbsp;Bandit&rangle;.

      An alternative, even more superior (read: even less overly strict) solution to the “late advancement to Chief Bandit” problem prohibits the &langle;Bandit&nbsp;&rarr;&nbsp;Chief&nbsp;Bandit&rangle; advancement, instead of the &langle;Rogue&nbsp;&rarr;&nbsp;Bandit&rangle; advancement, and then lumps Bandits in with “<b>Others</b>”. The reason for this is that permabandits do **not** get access to [Meso Guard](https://maplelegends.com/lib/skill?id=4211005) (and are naturally extremely short-reach).

      However, I am posing this in an overly strict version _just_ in case it’s necessary for reasons unknown to me, and the implementors (read: MapleLegends staff) can decide if loosening is possible, and then loosen it if so.
- <b>Q:</b> I don’t care.
    - <b>A:</b> That’s not a question, but okay. I will not attempt to convince the reader to care if they refuse to be convinced; I will simply note that this oversight severely discriminates against an important part of the playerbase for no reason, and thus will be seen as a severe blow directly inflicted upon them, with the only _apparent_ reason being that they are simply neither wanted nor welcome on this server. If that is an acceptable outcome, then so be it.

<details>
<summary>Footnotes</summary>

\*This exploit is certainly worth eliminating, although it’s also worth noting that it’s hard to really benefit from. At this rate, the bandit/CB/shad would rather just HP wash than spend _even more_ resources (including lots &amp; lots of SP, something that these classes don’t have many, if any, to spare) just to get a modest MAXHP boost! You would also have to do 2nd job advancement at about level &ap;180 at the latest, if you actually wanted a substantial benefit without the drawback of giving up MAXHP gains from higher-level challenge completions.

</details>

### Part 2: Clarify the HP thresholds

Currently, the HP thresholds look like this, according to [the official source](https://forum.maplelegends.com/index.php?threads/alpha-test-challenges-system-hp-wash-alternative.47420/):

| Levels | Mages | Warriors |   Bucc |   Shad | Others |
| -----: | ----: | -------: | -----: | -----: | -----: |
|    100 | 1,520 |    7,000 |  5,600 |  3,200 |  3,200 |
|    110 | 1,720 |    8,350 |  6,750 |  3,700 |  3,950 |
|    120 | 1,930 |    9,850 |  8,050 |  4,300 |  4,800 |
|    130 | 2,150 |   11,500 |  9,500 |  5,000 |  5,750 |
|    140 | 2,380 |   13,300 | 11,100 |  5,800 |  6,800 |
|    150 | 2,620 |   15,250 | 12,850 |  6,700 |  7,950 |
|    160 | 2,870 |   17,350 | 14,750 |  7,700 |  9,200 |
|    170 | 3,130 |   19,600 | 16,800 |  8,800 | 10,550 |
|    180 | 3,400 |   22,000 | 19,000 | 10,000 | 12,000 |

This is actually **totally fine as-is**. We just have to be clear what the headers of each column mean:

<details>
<summary>Table headers, clarified</summary>

- “<b>Mages</b>” encompasses the following classes:
    - <b>1st job:</b>
        - Magician.
    - <b>2nd job:</b>
        - Wizard (Fire/Poison).
        - Wizard (Ice/Lightning).
        - Cleric.
    - <b>3rd job:</b>
        - Mage (Fire/Poison).
        - Mage (Ice/Lightning).
        - Priest.
    - <b>4th job:</b>
        - Arch Mage (Fire/Poison).
        - Arch Mage (Ice/Lightning).
        - Bishop.
- “<b>Warriors</b>” encompasses the following classes:
    - <b>1st job:</b>
        - Swordman.
    - <b>2nd job:</b>
        - Fighter.
        - Page.
        - Spearman.
    - <b>3rd job:</b>
        - Crusader.
        - White Knight.
        - Dragon Knight.
    - <b>4th job:</b>
        - Hero.
        - Paladin.
        - Dark Knight.
- “<b>Bucc</b>” encompasses the following classes:
    - <b>2nd job:</b>
        - Brawler.
    - <b>3rd job:</b>
        - Marauder.
    - <b>4th job:</b>
        - Buccaneer.
- “<b>Shad</b>” encompasses the following classes:
    - <b>2nd job:</b>
        - Bandit\*.
    - <b>3rd job:</b>
        - Chief Bandit.
    - <b>4th job:</b>
        - Shadower.
- “<b>Others</b>” encompasses the following classes:
    - <b>0th job:</b>
        - Beginner.
    - <b>1st job:</b>
        - Archer.
        - Rogue.
        - Pirate.
    - <b>2nd job:</b>
        - Hunter.
        - Crossbowman.
        - Assassin.
        - Gunslinger.
    - <b>3rd job:</b>
        - Ranger.
        - Sniper.
        - Hermit.
        - Outlaw.
    - <b>4th job:</b>
        - Bow Master.
        - Marksman.
        - Night Lord.
        - Corsair.

</details>

\*Bandits _ideally_ should be in the “<b>Others</b>” category, although this requires the job-advancement caveat suggested in the previous section.

Here’s a little Q&amp;A for part 2:

- <b>Q:</b> Doesn’t this award too much MAXHP to permabeginners who complete the challenges?
    - <b>A:</b> This would put the typical permabeginner on par with, say, a completely unwashed archer. Being on par with an unwashed archer is not exactly “massive survivability”, especially when you’re a permabeginner, and thus largely rely on being _extremely_ close to your opponent at all times in order to deal damage. It _is_ true that permabeginners have naturally smaller MAXHP pools than archers do in the absence of both HP washing and the Challenge System; however, this basically just pans out (in pre-Challenge-System MapleLegends) as “permabeginners just have to wash _way_ harder than anyone else”. If permabeginners _really absolutely need_ a nerf here, then the “<b>Shad</b>” category can simply be expanded to “<b>Shad or beginner</b>”.
