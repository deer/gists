\*Probably unimportant, but included for completeness.

†Because I play ML on HD2.

‡My `dxvk.conf` pretty much just has:

```
dxgi.maxFrameLatency = 1
d3d9.maxFrameLatency = 1
dxgi.maxFrameRate = 70
d3d9.maxFrameRate = 70
```

Capping the framerate at 70 is purely a failsafe measure related to the <kbd><kbd>Alt</kbd>+<kbd>Enter</kbd></kbd> thing mentioned below. In reality, the game will be capped at 60 FPS anyways, but the cap at 70 is there to prevent runaway framerates when that 60 FPS cap isn’t enforced (read: when the client is very first launched).

- Game options
    - Executable: `.../MapleLegends/MapleLegends.exe`
    - Working directory: `.../MapleLegends/`
    - Prefix architecture: 32-bit
- Runner options
    - Wine version: lutris-GE-Proton8-8-x86\_64
    - \*Use system winetricks: yes
    - DXVK
        - Enable DXVK: yes
        - DXVK version: v2.3
    - \*VKD3D
        - Enable VKD3D: yes
        - VKD3D version: v2.10
    - \*D3D Extras
        - Enable D3D Extras: yes
        - D3D extras version: v2
    - DXVK-NVAPI / DLSS
        - Enable DXVK-NVAPI / DLSS: no
    - dgvoodoo2
        - Enable dgvoodoo2: no
    - Enable Esync: no
    - Enable Fsync: yes
    - Enable AMD FidelityFX Super Resolution (FSR): no
    - Enable BattlEye Anti-Cheat: no
    - Enable Easy Anti-Cheat: no
    - Virtual desktop
        - Windowed (virtual desktop): yes
        - †Virtual desktop resolution: 1366x768
    - \*DPI
        - Enable DPI scaling: no
    - \*Mouse Warp Override: Enable
    - \*Audio driver: auto
    - DLL overrides
        - `d3d8`=`n`
        - `d3d9`=`n`
    - \*Output debugging info: Disabled
    - \*Show crash dialogs: no
    - \*Autoconfigure joypads: no
    - \*Sandbox
        - Create a sandbox for Wine folders: yes
- System options
    - Lutris
        - Disable Lutris Runtime: yes
        - Prefer system libraries: yes
    - \*Gamescope \[<i>disabled</i>\]
    - \*CPU \[<i>all default</i>\]
    - \*Display \[<i>all default</i>\]
    - \*Audio \[<i>all default</i>\]
    - \*Input \[<i>all default</i>\]
    - \*Multi-GPU \[<i>all default</i>\]
    - \*Text based games \[<i>all default</i>\]
    - Game execution
        - Environment variables
            - ‡`DXVK_CONFIG_FILE`=`.../dxvk.conf`
            - \*`mesa_glthread`=`true`
            - \*`vblank_mode`=`2`
    - \*Xephyr (Deprecated, use Gamescope) \[<i>all default</i>\]

I have d3d8to9 (<https://github.com/crosire/d3d8to9/releases/latest>) plopped right into `.../MapleLegends/`. The way that I have it set up basically requires that I do a quick <kbd><kbd>Alt</kbd>+<kbd>Enter</kbd></kbd> each time that I launch a new client, because I’m no longer using [DxWrapper](https://github.com/elishacloud/dxwrapper). Slightly annoying, but I consider it to be worth it, because I know that I’m always using the latest version of d3d8to9.
