# Application to join all-odd Zakum runs!!

All fields are required unless otherwise specified.

If you like, you may use the raw [Markdown](https://en.wikipedia.org/wiki/Markdown) that makes up this document as a template for your application (it should work in, for example, Discord™).

- **Your name (what you prefer to be called):**
- **Your IGN(s) that is/are applying, along with each char’s job \& level:**
- **Other IGNs that you play actively (level \& job are optional), if any:**
- **Your guild, if any:**
- **Did you do the Zakum prequests? (yes/no):**
- **What would you say your character brings to the table (feel free to make use of vivid imagery)?:**
- **Any additional restrictions that your character has that are non-obvious, if any:**
- **Roughly what is your MAXHP, unbuffed?:**
- **Is Zakum the crumbly stone statue, or the tree behind it?:**
- **What’s your timezone/availability like?:**
- **How queer are you, on a scale from “1” to “all-odd Zak”?:**
- **How do you feel about all-odd Zak being experimental, given that we’ve literally never even attempted it?:**
- **Your Discord™ identifier (optional):**
- **Are you one of those people who complains when the Zak run takes 31 minutes instead of 30? (yes/no):**
- **What would 69 + 420 be, if you were bad at maths (this one is just to filter out the bots)?:**
- **Any additional considerations, or anything else you’d like to add \>:\] ?:**

Tee hee~
