# Keeping some writing systems &amp; languages straight because I cannot keep this all in my head

A note to myself. If you’re not me, then you should probably know that the `lang` attribute is frequently necessary to get text to display properly, to get text to read properly, &amp; arguably to get the correct semantics as well. This table is obviously not exhaustive, &amp; at the moment, also doesn’t represent anything not considered broadly “CJK”.

---

Early Chinese scripts aren’t encoded in Unicode, &amp; so aren’t represented here.

Bopomofo should be written vertically to the side of the main character when used as rubi text. Other rubi text can be written horizontally, underneath and/or over the main text.

<table>
  <thead>
    <tr>
      <th scope="col">lect</th>
      <th scope="col">region</th>
      <th scope="col">writing system</th>
      <th scope="col">scheme</th>
      <th scope="col"><code>lang</code></th>
      <th scope="col">rubi</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td colspan="2" rowspan="2">Old Korean</td>
      <td colspan="2">idu, hyangchal</td>
      <td><code>oko-Hani</code></td>
      <td>hangeul</td>
    </tr>
    <tr>
      <td colspan="2">*hangeul, *hangeul+hanja</td>
      <td><code>oko-Kore</code></td>
      <td>hangeul for hanja</td>
    </tr>
    <tr>
      <td colspan="2" rowspan="3">Middle Korean</td>
      <td colspan="2">hanja</td>
      <td><code>okm-Hani</code></td>
      <td>hangeul</td>
    </tr>
    <tr>
      <td colspan="2">hangeul, hangeul+hanja</td>
      <td><code>okm-Kore</code></td>
      <td>hangeul for hanja</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Yale</td>
      <td><code>okm-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="3">Korean</td>
      <td colspan="2">hangeul, hangeul+hanja</td>
      <td><code>ko</code></td>
      <td>hangeul for hanja</td>
    </tr>
    <tr>
      <td colspan="2">hanja</td>
      <td><code>ko-Hani</code></td>
      <td>hangeul</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Revised Romanisation</td>
      <td><code>ko-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="3">Old&nbsp;Japanese, Early&nbsp;Middle&nbsp;Japanese</td>
      <td colspan="2">man’yōgana</td>
      <td><code>ojp-Hani</code></td>
      <td>hiragana</td>
    </tr>
    <tr>
      <td colspan="2">hiragana, katakana</td>
      <td><code>ojp-Hrkt</code></td>
      <td rowspan="2"></td>
    </tr>
    <tr>
      <td>latin</td>
      <td>*Nihonsiki</td>
      <td><code>ojp-Latn</code></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Late Middle Japanese</td>
      <td colspan="2">Japanese</td>
      <td><code>ja</code></td>
      <td>hiragana for kanji</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Nihonsiki</td>
      <td><code>ja-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Japanese</td>
      <td colspan="2">Japanese</td>
      <td><code>ja</code></td>
      <td>hiragana for kanji</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Hepburn</td>
      <td><code>ja-Latn-hepburn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Old Chinese</td>
      <td colspan="2">*traditional</td>
      <td><code>och-Hant</code></td>
      <td>*IPA</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>*IPA</td>
      <td><code>och-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Late Old Chinese</td>
      <td colspan="2">traditional</td>
      <td><code>och-Hant</code></td>
      <td>*General Chinese</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>*General Chinese</td>
      <td><code>och-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Middle Chinese</td>
      <td colspan="2">traditional</td>
      <td><code>ltc-Hant</code></td>
      <td>General Chinese</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>General Chinese</td>
      <td><code>ltc-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Old&nbsp;Mandarin, Middle&nbsp;Mandarin</td>
      <td colspan="2">traditional</td>
      <td><code>cmn-Hant</code></td>
      <td>*bopomofo</td>
    </tr>
    <tr>
      <td rowspan="2">latin</td>
      <td>*Hàny&#x1d4; Pīnyīn</td>
      <td rowspan="2"><code>cmn-Latn-pinyin</code></td>
      <td rowspan="3"></td>
    </tr>
    <tr>
      <td rowspan="7">Standard Mandarin</td>
      <td rowspan="3">[<i>any</i>]</td>
      <td>Hàny&#x1d4; Pīnyīn</td>
    </tr>
    <tr>
      <td colspan="2">bopomofo</td>
      <td><code>cmn-Bopo</code></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="3">traditional</td>
      <td><code>cmn-Hant</code></td>
      <td rowspan="2">Hàny&#x1d4;&nbsp;Pīnyīn, bopomofo</td>
    </tr>
    <tr>
      <td>Hong&nbsp;Kong, Macao</td>
      <td><code>cmn-Hant-HK</code></td>
    </tr>
    <tr>
      <td>Taiwan</td>
      <td><code>cmn-Hant-TW</code></td>
      <td>bopomofo</td>
    </tr>
    <tr>
      <td>Singapore, Malaysia</td>
      <td colspan="2" rowspan="3">simplified</td>
      <td><code>cmn-Hans-SG</code></td>
      <td rowspan="2">Hàny&#x1d4; Pīnyīn</td>
    </tr>
    <tr>
      <td>mainland China</td>
      <td rowspan="2"><code>cmn-Hans</code></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Southwestern Mandarin<sup>[1]</sup></td>
      <td>Sichuanese Pinyin</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Sichuanese Pinyin</td>
      <td><code>cmn-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Lower Yangtze Mandarin<sup>[2]</sup></td>
      <td colspan="2">simplified</td>
      <td><code>cmn-Hans</code></td>
      <td><a href="https://en.wikipedia.org/wiki/User:%E6%9F%B3%E6%BC%AB/Romanization_of_Nankinese">Wiktionary</a></td>
    </tr>
    <tr>
      <td>latin</td>
      <td><a href="https://en.wikipedia.org/wiki/User:%E6%9F%B3%E6%BC%AB/Romanization_of_Nankinese">Wiktionary</a></td>
      <td><code>cmn-Latn</code></td>
      <td rowspan="3"></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Dungan</td>
      <td colspan="2">cyrillic</td>
      <td><code>dng-Cyrl</code>, <code>dng</code></td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Hàny&#x1d4;&nbsp;Pīnyīn, <a href="https://en.wiktionary.org/wiki/Wiktionary:Dungan_transliteration">Wiktionary</a></td>
      <td><code>dng-Latn-pinyin</code>, <code>dng-Latn</code></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">[<i>other Central Plains Mandarin</i>]<sup>[3]</sup></td>
      <td colspan="2">simplified</td>
      <td><code>cmn-Hans</code></td>
      <td>Hàny&#x1d4; Pīnyīn</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Hàny&#x1d4; Pīnyīn</td>
      <td><code>cmn-Latn-pinyin</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Jin</td>
      <td colspan="2">simplified</td>
      <td><code>cjy-Hans</code></td>
      <td><a href="https://en.wiktionary.org/wiki/Wiktionary:About_Chinese/Jin">Wiktionary</a></td>
    </tr>
    <tr>
      <td>latin</td>
      <td><a href="https://en.wiktionary.org/wiki/Wiktionary:About_Chinese/Jin">Wiktionary</a></td>
      <td><code>cjy-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td rowspan="3">Eastern Min</td>
      <td>mainland China</td>
      <td colspan="2">simplified</td>
      <td><code>cdo-Hans</code></td>
      <td rowspan="2">Bàng-uâ-cê</td>
    </tr>
    <tr>
      <td>Taiwan</td>
      <td colspan="2">traditional</td>
      <td><code>cdo-Hant-TW</code></td>
    </tr>
    <tr>
      <td>[<i>any</i>]</td>
      <td>latin</td>
      <td>Bàng-uâ-cê</td>
      <td><code>cdo-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Pu&ndash;Xian Min</td>
      <td colspan="2">simplified</td>
      <td><code>cpx-Hans</code></td>
      <td>Bá&#x207f;-uā-ci&#x30d;</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Bá&#x207f;-uā-ci&#x30d;</td>
      <td><code>cpx-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Central Min</td>
      <td colspan="2">simplified</td>
      <td><code>czo-Hans</code></td>
      <td>Kienning Colloquial Romanized</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Kienning Colloquial Romanized</td>
      <td><code>czo-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Northern Min</td>
      <td colspan="2">simplified</td>
      <td><code>mnp-Hans</code></td>
      <td>Kienning Colloquial Romanised</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Kienning Colloquial Romanised</td>
      <td><code>mnp-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="3">Taiwanese</td>
      <td colspan="2">traditional</td>
      <td><code>nan-Hant-TW</code></td>
      <td>bopomofo, Tâi-lô, POJ</td>
    </tr>
    <tr>
      <td colspan="2">bopomofo</td>
      <td><code>nan-Bopo-TW</code></td>
      <td rowspan="2"></td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Tâi-lô, POJ</td>
      <td><code>nan-Latn-TW-pehoeji</code></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="3">[<i>other Hokkien</i>], Datian&nbsp;Min<sup>[6]</sup></td>
      <td colspan="2">traditional</td>
      <td><code>nan-Hant</code></td>
      <td rowspan="2">POJ, Tâi-lô</td>
    </tr>
    <tr>
      <td colspan="2">simplified</td>
      <td><code>nan-Hans</code></td>
    </tr>
    <tr>
      <td>latin</td>
      <td>POJ, Tâi-lô</td>
      <td><code>nan-Latn-pehoeji</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Teochew</td>
      <td colspan="2">simplified</td>
      <td><code>nan-Hans</code></td>
      <td>Peng’im, PUJ</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Peng’im, PUJ</td>
      <td><code>nan-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Hainanese, Leizhou&nbsp;Min<sup>[5]</sup></td>
      <td colspan="2">simplified</td>
      <td><code>nan-Hans</code></td>
      <td>B&#x1fd;h-oe-tu, Hainanese Transliteration Scheme</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>B&#x1fd;h-oe-tu, Hainanese Transliteration Scheme</td>
      <td><code>nan-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Northern Pinghua</td>
      <td colspan="2">simplified</td>
      <td><code>cnp-Hans</code></td>
      <td>General Chinese</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>General Chinese</td>
      <td><code>cnp-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Southern Pinghua</td>
      <td colspan="2">simplified</td>
      <td><code>csp-Hans</code></td>
      <td>General Chinese</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>General Chinese</td>
      <td><code>csp-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Huizhou</td>
      <td colspan="2">simplified</td>
      <td><code>czh-Hans</code></td>
      <td>General Chinese</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>General Chinese</td>
      <td><code>czh-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Gan</td>
      <td colspan="2">simplified</td>
      <td><code>gan-Hans</code></td>
      <td>Pinfa</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Pinfa</td>
      <td><code>gan-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Taiwanese Hakka<sup>[4]</sup></td>
      <td colspan="2">traditional</td>
      <td><code>hak-Hant-TW</code></td>
      <td>Pha&#x30d;k-fa-s&#x1e73;</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Pha&#x30d;k-fa-s&#x1e73;</td>
      <td><code>hak-Latn-TW</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Meixian Hakka<sup>[4]</sup></td>
      <td colspan="2">simplified</td>
      <td><code>hak-Hans-CH</code></td>
      <td>Pinfa</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Pinfa</td>
      <td><code>hak-Latn-CH</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="3">[<i>other Hakka</i>]<sup>[4]</sup></td>
      <td colspan="2">traditional</td>
      <td><code>hak-Hant</code></td>
      <td>Pha&#x30d;k-fa-s&#x1e73;, Pinfa</td>
    </tr>
    <tr>
      <td colspan="2">simplified</td>
      <td><code>hak-Hans</code></td>
      <td>Pinfa, Pha&#x30d;k-fa-s&#x1e73;</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Pinfa, Pha&#x30d;k-fa-s&#x1e73;</td>
      <td><code>hak-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Xiang</td>
      <td colspan="2">simplified</td>
      <td><code>hsn-Hans</code></td>
      <td>Pinfa</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Pinfa</td>
      <td><code>hsn-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">Oujiang, Wenzhounese<sup>[7]</sup></td>
      <td colspan="2">simplified</td>
      <td><code>wuu-Hans</code></td>
      <td>Wenzhouhua</td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Wenzhouhua</td>
      <td><code>wuu-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="2">[<i>other Wu</i>]</td>
      <td colspan="2">simplified</td>
      <td><code>wuu-Hans</code></td>
      <td>Wugniu<sup>[8]</sup></td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Wugniu<sup>[8]</sup></td>
      <td><code>wuu-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="3">Taishanese<sup>[9]</sup></td>
      <td colspan="2">simplified</td>
      <td><code>yue-Hans</code></td>
      <td rowspan="2"><a href="https://en.wiktionary.org/wiki/Wiktionary:About_Chinese/Cantonese/Taishanese">Wiktionary</a></td>
    </tr>
    <tr>
      <td colspan="2">traditional</td>
      <td><code>yue-Hant</code></td>
    </tr>
    <tr>
      <td>latin</td>
      <td><a href="https://en.wiktionary.org/wiki/Wiktionary:About_Chinese/Cantonese/Taishanese">Wiktionary</a></td>
      <td><code>yue-Latn</code></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2" rowspan="3">Cantonese, [<i>other Yue</i>]<sup>[9]</sup></td>
      <td colspan="2">simplified</td>
      <td><code>yue-Hans</code></td>
      <td rowspan="2">Jyutping</td>
    </tr>
    <tr>
      <td colspan="2">traditional</td>
      <td><code>yue-Hant</code></td>
    </tr>
    <tr>
      <td>latin</td>
      <td>Jyutping</td>
      <td><code>yue-Latn-jyutping</code></td>
      <td></td>
    </tr>
  </tbody>
  <tfoot>
    <tr>
      <th scope="col">lect</th>
      <th scope="col">region</th>
      <th scope="col">writing system</th>
      <th scope="col">scheme</th>
      <th scope="col"><code>lang</code></th>
      <th scope="col">rubi</th>
    </tr>
  </tfoot>
</table>

## Footnotes

1. ISO 639-6 had `xghu` “Southwestern Mandarin”, but was withdrawn. Southwestern Mandarin has very limited mutual intelligibility with Standard Mandarin &amp; generally varies quite a bit in vocabulary.
2. ISO 639-6 had `juai` “Lower Yangtze Mandarin”, but was withdrawn. See: <sup>\[1\]</sup>.
3. ISO 639-6 had `zgyu` “Zhongyuan Mandarin”, but was withdrawn. See: <sup>\[1\]</sup>.
4. ISO 639-6 had `htia` “Taiwanese Hakka”, but was withdrawn. The Hakka lects are closely related, but have some mutually unintelligible varieties. The <i>Meixian</i> &amp; <i>Sixian dialects</i> are minor sublects of the prestige lect, with the Sixian being associated with Taiwan.
5. Hainanese &amp; Leizhou Min are frequently not considered to be Southern Min lects, but ISO 639-3 gives no choice but `nan`.
6. Datian Min may or may not be a Southern Min lect, depending on whom you ask.
7. ISO 639-6 had `qjio` “Oujiang” &amp; `wzhu` “Wenzhounese”, but was withdrawn. Oujiang/Wenzhounese is not mutually intelligible with any other Wu lects.
8. See: [<cite>Romanization of Wu Chinese</cite>](https://en.wikipedia.org/wiki/Romanization_of_Wu_Chinese). Wugniu is the most common these days, &amp; is used by Wiktionary. However, like the other Romanisation schemes, it fails to make some distinctions. What can you do.
9. ISO 639-3 does not make distinctions internally within `yue`, despite the lack of mutual intelligibility. ISO 639-6 had `tisa` “Taishanese”, but was withdrawn.
